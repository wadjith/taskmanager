import React from "react";

class CreateTask extends React.Component {
    constructor(props){ 
        super(props);
        this.state = { 
            value: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
     }
    
    handleChange(event) {
        this.setState({ value: event.target.value });

    }

    handleSubmit(event) {
        event.preventDefault();
        let newId = (this.props.tasks.length === 0) ? 1 : this.props.tasks[0].id + 1
        let task = {id: newId, label: this.state.value, status: "nouveau"}
        this.props.fnAddTask(task);
    }

    render() {
        return(
            <div>
                <h4>Créer une nouvelle tâche</h4>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" id="tache" class="form-control" placeholder="Libellé de la tâche" required
                        onChange={this.handleChange} value={this.state.value} />
                    <button class="btn btn-default">
                        Enregistrer
                    </button>
                    
                </form>
    
            </div>
            
        );
    }

    
}
export default CreateTask;