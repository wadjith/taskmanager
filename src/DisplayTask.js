function DisplayTask(props) {
    const { theTask, isSelected, fnAddSelectedTask, fnRemoveSelectedTask } = props;
    
    function handleChange(event){
        /*if(event.target.checked) {
            fnAddSelectedTask(event.target.value)
        }*/
        let fn = (event.target.checked) ? fnAddSelectedTask : fnRemoveSelectedTask;
        fn(event.target.value);
    }
    return(
        <tr>
            <td>
                <input type="checkbox" name = {theTask.id} value = {theTask.label} checked = { isSelected } onChange = {handleChange} />
            </td>
            <td>
                {theTask.label}
            </td>
            <td>
                {theTask.status}
            </td>
        </tr>
    );
}

export default DisplayTask;