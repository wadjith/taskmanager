import React from "react";
import DisplayTask from "./DisplayTask";

function ListTask(props){
    const { filter, tasks, selectedLabel, fnAddSelectedTask, fnRemoveSelectedTask } = props;

    return(
        <div>
            <h2>Liste des tâches</h2>
            <table class="table table-bordered">
                <thead>
                    <th></th>
                    <th>Label</th>
                    <th>Status</th>
                </thead>
                <tbody>
                { tasks.filter(task => task.label.includes(filter))
                    .map(task => <DisplayTask key={task.id} theTask={task} 
                        isSelected={ (selectedLabel.indexOf(task.label) === -1) ? false : true }
                        fnAddSelectedTask={fnAddSelectedTask} fnRemoveSelectedTask={fnRemoveSelectedTask} />) }
                </tbody>
                
            </table>
            
        </div>
        
    );
}
/*
class ListTask extends React.Component{
    constructor(props){
        super(props);
        this.state = {};
    }

    render(){
        return(
            <div>
                <h2>Liste des tâches</h2>
                { this.props.tasks.filter(task => task.label.includes(this.props.filter))
                        .map(task => <DisplayTask key={task.id} theTask={task} 
                            isSelected={ (this.props.selectedLabel.indexOf(task.label) === -1) ? false : true }
                            fnAddSelectedTask={this.props.fnAddSelectedTask} fnRemoveSelectedTask={this.props.fnRemoveSelectedTask} />) }
            </div>
            
        );
    }
} */
export default ListTask;