let taskList = [
    {id: 6, label: "Prendre un seau", status: "Terminer"},
    {id: 5, label: "Remplir l'eau dans le seau", status: "Terminer"},
    {id: 4, label: "Verser l'eau sur son corps", status: "En cours"},
    {id: 3, label: "Metrre le savon sur le corps", status: "En cours"},
    {id: 2, label: "Frotter son corps", status: "Nouveau"},
    {id: 1, label: "Rincer avec de l'eau", status: "Nouveau"}
];

export default taskList;