function SearchTask(props) {
    const { filter, fnChangeFilter } = props;
    
    function handleChange(event) {
        let filter = event.target.value;
        fnChangeFilter(filter);
    }

    return (
        <div>
            <h4>Retrouver des tâches</h4>
            <input type="text" id="libelle" class="form-control" placeholder="Entrer un mot clé"
                onChange={ handleChange } value={ filter } />
        </div>
        
    );
}
export default SearchTask;