import React from "react";
//import taskList from "./tasks";
import SearchTask from "./SearchTask";
import ListTask from "./ListTask";
import TaskStatusUpdate from "./TaskStatusUpdate";
import CreateTask from "./CreateTask";

class TaskManager extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      tasks: [],
      selectedTaskLabel: [],
      filterLabel: "",
    };

    this.changeTask = this.changeTask.bind(this);
    this.changeFilterLabel = this.changeFilterLabel.bind(this);
    this.addTask = this.addTask.bind(this);
    this.addSelectedTask = this.addSelectedTask.bind(this);
    this.removeSelectedTask = this.removeSelectedTask.bind(this);
    this.updateStatusSelectedTask = this.updateStatusSelectedTask.bind(this);
  }

  componentDidMount() {
    //this.changeTask(taskList);
  }
  componentWillUnmount() {

  }

  changeTask(taskList) {
    this.setState({ tasks: taskList });
  }

  addTask(task) {
    if (this.state.tasks.map(task => task.label).includes(task.label)) {
      alert("La tâche est déjà enregistrée !!")
    } 
    else {
      const myTasks = [task,...this.state.tasks];
      this.changeTask(myTasks);
    }
    
  }

  addSelectedTask(label) {
    this.setState(state => ({
      selectedTaskLabel: [label,...state.selectedTaskLabel]
    }));
  }

  removeSelectedTask(label) {
    let selectedLabel = this.state.selectedTaskLabel;
    let index = selectedLabel.indexOf(label);
    selectedLabel.splice(index, 1);
    this.setState({selectedTaskLabel: selectedLabel});
    
  }

  changeFilterLabel(label) {
    this.setState({ filterLabel: label });
  }

  updateStatusSelectedTask(status) {
    let myTasks = this.state.tasks;
    this.state.selectedTaskLabel.forEach(label => {
      let index = myTasks.map(task => task.label).indexOf(label);
      myTasks[index].status = status;
    });
    this.changeTask(myTasks);
  }

  render() {
    let tasks = this.state.tasks;
    let filterLabel = this.state.filterLabel;
    let selectedTaskLabel = this.state.selectedTaskLabel;

    return (
      <div>
        <h2>Task Manager</h2>
        <div class="row">
          <div class="col-lg-6 col-md-6">
            <SearchTask filter={filterLabel} fnChangeFilter={this.changeFilterLabel} />
          </div>
          <div class="col-lg-6 col-md-6">
            <CreateTask tasks={ tasks } fnAddTask={this.addTask} />
          </div>
        </div>

        <ListTask filter={filterLabel} tasks={ tasks } selectedLabel={ selectedTaskLabel }
          fnAddSelectedTask={this.addSelectedTask} fnRemoveSelectedTask={this.removeSelectedTask} />

        <TaskStatusUpdate selectedTaskLabel={ selectedTaskLabel } fnUpdateStatusSelectedTask={this.updateStatusSelectedTask} />

        
      </div>

    );
  }
}
export default TaskManager;